package org.lduriez.jcache.config;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.annotation.CacheKey;
import javax.cache.configuration.MutableConfiguration;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.lduriez.jcache.model.CityDto;

@ApplicationScoped
@Named("jcacheConfig")
public class JCacheConfig {

	public CacheManager getCacheManager() throws URISyntaxException {
		// Load configuration from an absolute filesystem path
		//URI uri = URI.create("test-infinispan-cache-cluster.xml");
		// Load configuration from a classpath resource
		// URI uri =
		// this.getClass().getClassLoader().getResource("test-infinispan-cache-cluster.xml").toURI();

		// Retrieve the system wide cache manager
		//CacheManager cacheManager = Caching.getCachingProvider().getCacheManager(uri, this.getClass().getClassLoader(), null);
		/*
		 * MutableConfiguration<CacheKey, List<CityDto>> config = new
		 * MutableConfiguration<>();
		 * config.setManagementEnabled(true).setStatisticsEnabled(true);
		 * config.setStoreByValue(false);
		 */
		// Define a named cache with default JCache configuration
		/*Iterable<String> iter = cacheManager.getCacheNames();
		for (String cacheName : iter) {
			System.out.println("Cache name = " + cacheName);
		}*/
		
		
		ClassLoader tccl = Thread.currentThread().getContextClassLoader();
		CacheManager cacheManager1 = Caching.getCachingProvider().getCacheManager(
		      URI.create("test-infinispan-cache-cluster.xml"), new TestClassLoader(tccl));
		CacheManager cacheManager2 = Caching.getCachingProvider().getCacheManager(
		      URI.create("test-infinispan-cache-cluster.xml"), new TestClassLoader(tccl));

		Cache<String, String> cache1 = cacheManager1.getCache("jcache-city-cache");
		Cache<String, String> cache2 = cacheManager2.getCache("jcache-city-cache");

		cache1.put("hello", "world");
		String value = cache2.get("hello"); // Returns "world" if clustering is working
		System.out.println("value = " + value);
		return cacheManager1;
	}

	public static class TestClassLoader extends ClassLoader {
		public TestClassLoader(ClassLoader parent) {
			super(parent);
		}
	}
}
