package org.lduriez.jcache.model;

import java.io.Serializable;

/**
 * DTO representing a city
 *
 */
public class CityDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * City name
	 */
	private String name;

	
	/**
	 * Constructor
	 * @param name City name
	 */
	public CityDto(String name) {
		super();
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

}
