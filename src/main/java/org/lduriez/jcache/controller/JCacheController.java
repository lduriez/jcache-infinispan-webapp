package org.lduriez.jcache.controller;

import java.util.ArrayList;
import java.util.List;

import javax.cache.annotation.CacheResult;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import org.lduriez.jcache.model.CityDto;

/**
 * Controller for JCache testing
 *
 */
@Named("jcacheController")
@RequestScoped
public class JCacheController {

	/**
	 * Fetch cities from datastore
	 * 
	 * @return all cities, unsorted
	 */
	@CacheResult(cacheName = "jcache-city-cache")
	public List<CityDto> getCities() {
		System.out.println("Fetching cities...");
		List<CityDto> cities = new ArrayList<>();
		cities.add(new CityDto("Paris"));
		cities.add(new CityDto("Suresnes"));
		return cities;
	}

	@CacheResult(cacheName = "jcache-city-cache")
	public String getCapital() {
		System.out.println("Fetching capital...");
		return "Paris";
	}
}
