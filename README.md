jcache-infinispan-webapp: WIP Test Infinispan configured with JCache API
=====================================
Author: Lionel Duriez

Tested on
--------------------
JBoss EAP 7.2 with Infinispan Wildfly modules 10.1.7.Final

Install Infinispan Wildfly Modules
--------------------

Download from https://infinispan.org/download/

Copy modules directory found in the archive to $JBOSS_HOME directory. 

Deploy application on two JBoss servers
---------------------

Server 1 : localhost:8080

Server 2 : localhost:8180

Access the application
---------------------

Open http://localhost:8080/infinispan-webapp-0.0.1-SNAPSHOT/main/infinispan.jsp => City cache size 0

Open http://localhost:8080/infinispan-webapp-0.0.1-SNAPSHOT/main/infinispan-cities.jsp => Cities are displayed

Open http://localhost:8080/infinispan-webapp-0.0.1-SNAPSHOT/main/infinispan.jsp => City cache size 2

Open http://localhost:8180/infinispan-webapp-0.0.1-SNAPSHOT/main/infinispan.jsp => City cache size 2

Open http://localhost:8180/infinispan-webapp-0.0.1-SNAPSHOT/main/infinispan-cities.jsp => Cities are displayed

TODO
--------------------

1. Broken, fix it
